import csv
IN_FILE = '/home/roberto/G-Strings/cp_2017/benchmarks/results.csv'
reader = csv.reader(open(IN_FILE), delimiter = '|')
results = {}
for row in reader:
  inst = row[0]
  length = int(row[1])
  solver = row[2].upper()
  if solver == 'GECODE+S':
    solver = '_GECODE+S'
  elif solver == 'G-STRINGS':
    solver = '__G-STRINGS'
  info = row[4]
  if inst not in results.keys():
    results[inst] = {}
  if solver not in results[inst].keys():
    results[inst][solver] = {}
  if info in ['sat','uns']:
    results[inst][solver][length] = float(row[3])
  elif info == 'out':
    results[inst][solver][length] = 'T/O'
  else:
    results[inst][solver][length] = 'N/A'
OUT_FILE = '/home/roberto/G-Strings/cp_2017/benchmarks/table.csv'
writer = csv.writer(open(OUT_FILE, 'w'), delimiter = '|')
writer.writerow(sorted(results['sql'].keys()))
for inst, item in sorted(results.items()):
  row = []
  row += [inst]
  for solver, times in sorted(item.items()):
    row += [x[1] for x in sorted(times.items())]
  writer.writerow(row)